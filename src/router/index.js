import Vue from 'vue'
import Router from 'vue-router'
import AllGeoSport from '@/components/All.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'AllGeoSport',
      component: AllGeoSport
    }
  ]
})
